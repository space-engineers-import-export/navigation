﻿using Sandbox.ModAPI.Ingame;
using System.Collections.Generic;
using IngameScript.Configuration;
using IngameScript.Configuration.Builder;

namespace IngameScript
{
    public class TravelTarget
    {
        public TravelTarget() { }

        public static SectionConfigDefinition<TravelTarget> CONFIGURATION_DEFINITION =
            ConfigItemDefinitionBuilder.ofSectionConfig(() => new TravelTarget(), "Navigation Target ", "", (TravelTarget newInstance, object fieldValue) => newInstance.name = (string)fieldValue)
               .addField("travelSpeed", ConfigFieldTypes.FLOAT, 100f, (TravelTarget newInstance, object fieldValue) => newInstance.travelSpeed = (float)fieldValue)
               .addField("approachSpeed", ConfigFieldTypes.FLOAT, 10f, (TravelTarget newInstance, object fieldValue) => newInstance.approachSpeed = (float)fieldValue)
               .addField("approachDistance", ConfigFieldTypes.FLOAT, 500f, (TravelTarget newInstance, object fieldValue) => newInstance.approachDistance = (float)fieldValue)
               .addField("waypoints", ConfigFieldTypes.WAYPOINT_LIST, (TravelTarget newInstance, object fieldValue) => {
                   var temp = (List<object>)fieldValue;
                   var list = new List<MyWaypointInfo>();
                   foreach(var t in temp)
                   {
                       list.Add((MyWaypointInfo)t);
                   }
                   newInstance.waypoints = list;
               })
               .build();

        public List<MyWaypointInfo> waypoints;
        public string name;
        public float travelSpeed;
        public float approachSpeed;
        public float approachDistance;
        public MyWaypointInfo lastWaypoint { get { return this.waypoints[this.waypoints.Count - 1]; } private set { } }
    }
}
