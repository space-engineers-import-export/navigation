﻿using IngameScript.Configuration;
using IngameScript.Menu;
using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private const string LOCATION_SECTION = "courses";
        private const string WAYPOINT_POSTFIX = ".waypoints";
        private const string NAME_POSTFIX = ".name";
        private const string TRAVEL_SPEED_POSTFIX = ".travelspeed";
        private const string APPROACH_SPEED_POSTFIX = ".approachspeed";
        private const string APPROACH_DISTANCE = ".approachdistance";
        private const string DISPLAY_SECTION = "displays";
        private const string INDEX_POSTFIX = ".display";

        private MyIni ini = new MyIni();

        private IMyRemoteControl controlBlock;
        private List<TravelTarget> targets;
        private List<IMyTextSurface> displays;

        private TravelTarget target;

        private MenuSystem menu;

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update10;

            var controlBlocks = new List<IMyRemoteControl>();
            GridTerminalSystem.GetBlocksOfType(controlBlocks, b => Me.IsSameConstructAs(b));

            if (controlBlocks.Count != 1)
            {
                Echo("only one RemoteControl Block allowed");
                throw new Exception();
            }

            controlBlock = controlBlocks[0];

            setupTargets();
        }

        private void setupTargets()
        {
            ini.TryParse(Me.CustomData);

            target = null;

            targets = ConfigLoader.load(TravelTarget.CONFIGURATION_DEFINITION, ini,Echo);

            displays = new List<IMyTextSurface>();

            for (var i = 0; ini.ContainsKey(DISPLAY_SECTION, i + NAME_POSTFIX); i++)
            {
                var displayName = ini.Get(DISPLAY_SECTION, i + NAME_POSTFIX).ToString();
                var surfaceProvider = GridTerminalSystem.GetBlockWithName(displayName) as IMyTextSurfaceProvider;
                if (surfaceProvider == null)
                {
                    Echo("Could not find Display Block " + displayName);
                    continue;
                }

                var displayIndex = ini.Get(DISPLAY_SECTION, i + INDEX_POSTFIX).ToInt16();
                if (displayIndex >= surfaceProvider.SurfaceCount)
                {
                    Echo("Illegal display count " + displayIndex + " in display configuration nr " + i);
                    continue;
                }
                displays.Add(surfaceProvider.GetSurface(displayIndex));
            }

            setupMenu();

            Echo("initialized " + targets.Count + " targets and " + displays.Count + " displays");
        }

        private void setupMenu()
        {
            MenuFolder folder = new MenuFolder("root");
            folder.addMenuItem(new MenuAction("Reload", delegate () { setupTargets(); }));

            foreach (var target in targets)
            {
                folder.addMenuItem(new MenuAction(target.name, delegate () { approach(target); menu.close(); }));
            }

            menu = new MenuSystem(displays, folder);
        }

        void Main(string argument, UpdateType updateSource)
        {
            if (updateSource == UpdateType.Update10)
            {
                if (target != null)
                {
                    Echo("approaching " + target.name);
                    speedLimit();
                }

                printToDisplays();
                return;
            }

            menu.handleCommand(argument);
            if (argument.Length == 0)
            {
                setupTargets();
                return;
            }
            int targetIndex;
            if (!int.TryParse(argument, out targetIndex))
            {
                Echo("can not parse target index " + argument);
                return;
            }
            if (targetIndex >= targets.Count)
            {
                Echo("target index " + targetIndex + " not possible. Maximum is " + (targets.Count - 1));
                return;
            }
            approach(targetIndex);
        }

        private void printToDisplays()
        {
            if (!menu.printMenu())
            {
                string approaching;
                if (this.target != null)
                {
                    approaching = "Approaching: " + this.target.name + "\nDistance: " + targetDistanceString(this.target);
                }
                else
                {
                    approaching = "No Approach";
                }
                foreach (var display in displays)
                {
                    display.WriteText(approaching);
                }
            }
        }

        private string targetDistanceString(TravelTarget target)
        {
            return targetDistance(target).ToString("0") + "m";
        }

        private double targetDistance(TravelTarget target)
        {
            if (target.lastWaypoint.IsEmpty())
            {
                return double.MaxValue;
            }
            var currentPosition = controlBlock.CubeGrid.GridIntegerToWorld(controlBlock.Position);
            return Vector3D.Distance(target.lastWaypoint.Coords, currentPosition);
        }

        private void speedLimit()
        {
            if (!controlBlock.IsAutoPilotEnabled)
            {
                this.target = null;
                return;
            }
            var distance = targetDistance(target);
            if (distance < target.approachDistance)
            {
                controlBlock.SpeedLimit = target.approachSpeed;
            }
            else
            {
                Echo("Speedlimit in " + (distance - target.approachDistance).ToString("0") + "m");
            }
        }

        private void approach(TravelTarget target)
        {
            this.target = target;
            controlBlock.ClearWaypoints();
            foreach (var waypoint in target.waypoints)
            {
                controlBlock.AddWaypoint(waypoint);
            }
            controlBlock.SpeedLimit = target.travelSpeed;
            controlBlock.FlightMode = FlightMode.OneWay;
            controlBlock.SetAutoPilotEnabled(true);
        }

        private void approach(int targetIndex)
        {
            approach(targets[targetIndex]);
        }
    }
}
